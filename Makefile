# Build and run omer_count

TEST1=2023-04-05
PESACH=2023-04-06
TEST2=2023-04-07
TEST3=2023-04-08
TEST4=2023-05-09
TEST5=2023-05-20
SHAVUOT=2023-05-26
TEST6=2023-05-28

all : build test

build :
	@gprbuild -P ${.CURDIR}/omer_count.gpr
	@cp -f ${.CURDIR}/obj/omer_count ${.CURDIR}/

test :
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${TEST1}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${TEST1}
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${PESACH}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${PESACH}
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${TEST2}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${TEST2}
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${TEST3}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${TEST3}
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${TEST4}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${TEST4}
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${TEST5}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${TEST5}
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${SHAVUOT}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${SHAVUOT}
	@echo
	@echo ------------------------------------------------------------------------
	@echo "Test date ${TEST6}:"
	@echo
	@${.CURDIR}/omer_count --pesach ${PESACH} --for ${TEST6}

install : build
	@/usr/bin/install ${.CURDIR}/omer_count /usr/local/bin/omer_count

uninstall :
	@ls -lh /usr/local/bin/omer_count
	@rm -fi /usr/local/bin/omer_count

clean :
	@gprclean -P ${.CURDIR}/omer_count.gpr
	@gprclean -P ${.CURDIR}/libs/number-words/number_words.gpr


