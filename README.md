# Omer Count

A simple omer count program written in Ada 2012.

## TL;DR

In summary, on a Unix-like system such as BSD or GNU/Linux, you can
build and install the program by running:

```bash
$ make build
```

and then, as root:

```bash
make install
```

The program has been successfully built and tested on OpenBSD and
Debian GNU/Linux. It can also be compiled for Windows, and any other
platform supported by the libre software GNAT Ada compiler.


## What is an omer count and what is the purpose of this program?

There are two biblical feasts called Pesach and Shavuot. The latter is
fifty days after the former. There is a biblical commandment to count
the days, the weeks and the weekly Shabbats which occur between 16
Aviv (the day after Pesach) and the day of Shavuot, fifty days later.

The Bible gives the following instructions on how to count:

1. The counting is to begin on the day after the Pesach Shabbat; i.e.
   counting starts on 16 Aviv.
2. The days from one to forty-nine are to be counted, and then the
   fiftieth day is to be celebrated as the day of Shavuot. The days
   are counted, not the nights.
3. The celebration of the fiftieth day, being an annual Shabbat, is to
   begin at sunset of the previous day.
4. The seven weekly Shabbat days along the way are also to be counted.
5. Each of the seven weeks is to be counted.

We take it that to do this counting, the intention is that it be done
each day until the eve of Shavuot arrives. This program assists in
that daily counting exercise, helping to ensure that no error is made
along the way.


## Usage

```bash
# If the current day (today) is between Pesach and Shavuot, and the
# correct date of Pesach for the current year is set (hard coded, see
# *omer.ads*) within the compiled program, then the following
# suffices:
$ ./omer_count

# To get the Omer count for today, while specifying the correct date
# of Pesach:
$ ./omer_count --pesach 2019-03-23

# To get the Omer count for a specific date, while also specifying the
# correct date of Pesach:
$ ./omer_count --for 2019-05-12 --pesach 2019-03-23

# To get usage information and examples:
$ ./omer_count --help
```


## Building

In the top-level directory, on a Unix-like system, such as BSD or
GNU/Linux, run *make build* in your shell:

```bash
$ make build
```

This will compile and link the program to produce an executable file
for your platform. You should see output similar to the following
example:

```bash
Compile
   [Ada]          omer_count.adb
   [Ada]          english_cardinals_ordinals.adb
   [Ada]          eclecticse.ads
   [Ada]          eclecticse-date.adb
   [Ada]          omer.adb
   [Ada]          shavuot_banner.adb
Build Libraries
   [gprlib]       numberwords.lexch
   [archive]      libnumberwords.a
   [index]        libnumberwords.a
Bind
   [gprbind]      omer_count.bexch
   [Ada]          omer_count.ali
Link
   [link]         omer_count.adb
```

To build the program on Windows, use *gprbuild* directly, or load the
project file into the GNAT Studio IDE, and build from there.


## Testing

In the top-level directory, run *make test* in your shell:

```bash
$ make test
```

This will run a few tests on sample dates and print the output. You
should see output similar to the following example:

```bash
------------------------------------------------------------------------
Test date 2023-04-05:

It is not yet Pesach. The omer count has not yet begun.

------------------------------------------------------------------------
Test date 2023-04-06:

2023-04-06 is Pesach. Fifty more days to Shavuot.

------------------------------------------------------------------------
Test date 2023-04-07:

OMER COUNT FOR FRIDAY 2023-04-07

It is the first day of fifty to Shavuot.
There are forty-nine more days to go!
It is the first day of the first week since Pesach.
We have counted zero of the seven Shabbats!

------------------------------------------------------------------------
Test date 2023-04-08:

OMER COUNT FOR SHABBAT 2023-04-08

It is the second day of fifty to Shavuot.
There are forty-eight more days to go!
It is the second day of the first week since Pesach.
It is the first of the Shabbats!

------------------------------------------------------------------------
Test date 2023-05-09:

OMER COUNT FOR TUESDAY 2023-05-09

It is the thirty-third day of fifty to Shavuot.
There are seventeen more days to go!
It is the fifth day of the fifth week since Pesach.
We have counted five of the seven Shabbats!

------------------------------------------------------------------------
Test date 2023-05-20:

OMER COUNT FOR SHABBAT 2023-05-20

It is the forty-fourth day of fifty to Shavuot.
There are six more days to go!
It is the second day of the seventh week since Pesach.
It is the seventh of the Shabbats!

------------------------------------------------------------------------
Test date 2023-05-26:

Today (2023-05-26) is ...

SSSSSSS  H    H      A     V     V  U     U   OOOOO   TTTTTTT  
S        H    H     A A     V   V   U     U  O     O     T     
SSSSSSS  HHHHHH    A   A    V   V   U     U  O     O     T     
SSSSSSS  HHHHHH   AAAAAAA   V   V   U     U  O     O     T     
      S  H    H   A     A    V V    U     U  O     O     T     
SSSSSSS  H    H   A     A     V      UUUUU    OOOOO      T     


------------------------------------------------------------------------
Test date 2023-05-28:

Sorry, but you have missed Shavuot!
```


## Installing

On a Unix-like system, such as BSD or GNU/Linux, you can install
*omer_count* into */usr/local/bin/* by executing *make install* as
*root*:

```bash
make install
```

If you have built *omer_count* on Windows, or downloaded the
executable, just copy the executable to some directory that is on the
executable search PATH.


## Uninstalling

On a Unix-like system, such as BSD or GNU/Linux, you can uninstall
*omer_count* from */usr/local/bin/* by executing *make uninstall* as
*root*:

```bash
make uninstall
```

You will be prompted to confirm removal of the executable.

If you have built *omer_count* on Windows, or downloaded the
executable, just delete the executable.

