with Ada.Calendar;
with Ada.Strings.Bounded;
with Ada.Strings.Fixed;
with Ada.Strings.Maps;

-- @summary
-- A collection of date and time routines which operate on ISO 8601
-- date/time strings.
--
-- @description
-- This package provides routines to conveniently operate directly on
-- ISO 8601 dates and times in string format. At present, the focus is
-- on dates, but representation of times is also accommodated.
--
-- The logging of timestamps in ISO 8601 format is one of the
-- principal motivators for this package.
--
package Eclecticse.Date is

   package SF renames Ada.Strings.Fixed;
   package ASM renames Ada.Strings.Maps;
   package AS renames Ada.Strings;

   Bad_Year            : exception;
   Bad_Month           : exception;
   Bad_Day             : exception;
   Bad_Date_Difference : exception;

   -- Allow a bounded string length of 30 characters for a full ISO 8601 date/time.
   -- Example: 2016-11-16T17:28:30.4532+01:00
   ISO_Date_Length : Integer := 30;

   -- Create a bounded string type with which to represent ISO 8601 date/times.
   package ASB renames Ada.Strings.Bounded;
   package ISO_8601_Date_Time_Bounded is new ASB.Generic_Bounded_Length (ISO_Date_Length);
   package DTB renames ISO_8601_Date_Time_Bounded;
   use all type DTB.Bounded_String;

   subtype ISO_8601_Date_Time_String is DTB.Bounded_String;
   subtype ISO_8601_Date_String is DTB.Bounded_String;
   subtype ISO_8601_Time_String is DTB.Bounded_String;


   function Length (Source : ISO_8601_Date_Time_String) return DTB.Length_Range renames DTB.Length;

   function Index (Source : ISO_8601_Date_Time_String;
                   Set    : ASM.Character_Set;
                   Test   : AS.Membership;
                   Going  : AS.Direction := AS.Forward) return Natural renames DTB.Index;

   function Count
     (Source : ISO_8601_Date_Time_String;
      Set    : ASM.Character_Set) return Natural renames DTB.Count;


   -- The first day of the week is Sunday and not Monday.
   type DOW is (Sunday, Monday, Tuesday, Wednesday, Thursday, Friday, Saturday);

   -- UTC+10:00 is ten hours AHEAD of UTC.
   type TZ_Offset_Direction is (Ahead, Behind);

   -- Possible components of an ISO 8601 date/time string.
   type Date_Time_Component is (Year, Month, Day,
                                Hour, Minute, Second, Second_Fraction,
                                TZ_Direction, TZ_Hour, TZ_Minute);

   -- We will not be dealing with fictitious years. There was no year less than -5000.
   Low_Year  : constant := -5_000;

   -- We will not be dealing with fictitious years. There will be no Gregorian year more than 5000
   -- with which we will need to be concerned.
   High_Year : constant := 5_000;

   -- A year will always be represented with four digits.
   Year_Digits  : constant := 4;

   -- A month will always be represented with two digits.
   Month_Digits : constant := 2;

   -- A day will always be represented with two digits.
   Day_Digits   : constant := 2;

   -- An hour will always be represented with two digits.
   Hour_Digits   : constant := 2;

   -- A minute will always be represented with two digits.
   Minute_Digits : constant := 2;

   -- A second will always be represented with two digits.
   Second_Digits : constant := 2;

   -- Number of fraction digits in seconds fraction.
   Second_Fraction_Digits : constant := 4;


   -- Number of seconds in one hour. This does not take into account that an hour could be one second longer.
   Secs_In_Hour   : constant := 3_600;


   -- Number of seconds in one minute. This does not take into account that a minute could be one second longer.
   Secs_In_Minute : constant := 60;


   -- We will allow anachronistic Gregorian years to be represented.
   subtype Gregorian_Year_Number is Integer range Low_Year .. High_Year;


   -- Representing the Gregorian months from January to December.
   subtype Gregorian_Month_Number is Positive range 1 .. 12;


   -- Representing the number of days in any particular Gregorian month.
   subtype Gregorian_Day_Number is Positive range 1 .. 31;


   -- In twenty-four hour time, from midnight as zero hour.
   subtype Hour_Number is Natural range 0 .. 23;


   -- There are sixty minutes in an hour, but the first is the zero minute, and the last is fifty-nine.
   subtype Minute_Number is Natural range 0 .. 59;


   -- There are up to 61 seconds in a minute, but the first is the zero second, and the last is normally fifty-nine,
   -- except when there is a leap second added.
   subtype Second_Number is Natural range 0 .. 60;


   -- The number of days difference between two dates cannot be more than the number of days between the
   -- lowest year and the highest year.
   subtype Date_Diff_Days is Integer range
     Integer (Float (Low_Year - High_Year) * 365.25) .. Integer (Float (High_Year - Low_Year) * 365.25);


   -- Convert a string to bounded string.
   -- @param S A date, time, or date/time string of up to ISO_Date_Length characters.
   -- @return A bounded string with length limit of ISO_Date_Length characters. No validity checking is performed.
   function "+" (S : String) return DTB.Bounded_String;


   function "-" (S : DTB.Bounded_String) return String;


   function Date_Format_Okay (The_Date_String : ISO_8601_Date_String; With_Hyphens : Boolean := True) return Boolean;
   function Time_Format_Okay (The_Time_String : ISO_8601_Time_String; With_Colons : Boolean := True) return Boolean;


   -- This is just for year-month-day/hour-minute-second date/times with no fractional second and no timezone offset.
   function Date_Time_Format_Okay (The_Date_Time_String : ISO_8601_Date_Time_String;
                                   With_T_Sep           : Boolean := False;
                                   With_Hyphens         : Boolean := True;
                                   With_Colons          : Boolean := True)
                                   return Boolean;


   function Contains_Digits_Only (The_String : String) return Boolean;


   function Valid_ISO_8601_Date_Time_String (The_Date_Time_String : ISO_8601_Date_Time_String) return Boolean;


   -- Return the current date in ISO 8601 date format with or without separating hyphens.
   function ISO_8601_Date_Now (Hyphen : Boolean := True) return ISO_8601_Date_String
     with Post => (Date_Format_Okay (ISO_8601_Date_Now'Result, Hyphen) and
                       Valid_ISO_8601_Date_Time_String (ISO_8601_Date_Now'Result));


   -- Return the current time in ISO 8601 format with or without separating colons.
   function ISO_8601_Time_Now (Colon : Boolean := True) return ISO_8601_Time_String
     with Post => (Time_Format_Okay (ISO_8601_Time_Now'Result, Colon));


   -- Return the current date and time in ISO 8601 format with or without a "T" separator, with or without
   -- separating hyphens in the date portion, with or without separating colons in the time portion.
   function ISO_8601_Date_Time_Now (T_Sep  : Boolean := False;
                                    Hyphen : Boolean := True;
                                    Colon  : Boolean := True) return ISO_8601_Date_Time_String
     with Post => (Date_Time_Format_Okay (ISO_8601_Date_Time_Now'Result, T_Sep, Hyphen, Colon) and
                       Valid_ISO_8601_Date_Time_String (ISO_8601_Date_Time_Now'Result));


   -- Return an ISO 8601 date string for the given year, month and day, with or without the separating hyphens.
   function Make_ISO_8601_Date (Year   : Gregorian_Year_Number;
                                Month  : Gregorian_Month_Number;
                                Day    : Gregorian_Day_Number;
                                Hyphen : Boolean := True) return ISO_8601_Date_String
     with Post => (Date_Format_Okay (Make_ISO_8601_Date'Result, Hyphen) and
                       Valid_ISO_8601_Date_Time_String (Make_ISO_8601_Date'Result));


   -- Return an ISO 8601 time string for the given hour, minute and second, with or without the separating colons.
   function Make_ISO_8601_Time (Hour    : Hour_Number;
                                Minute  : Minute_Number;
                                Second  : Second_Number;
                                Colon   : Boolean := True) return ISO_8601_Time_String
     with Post => (Time_Format_Okay (Make_ISO_8601_Time'Result, Colon));


   -- Return an ISO 8601 date/time string for the given year, month, day, hour, minute and second,
   -- with or without the "T" separator, with or without the hyphen separators in the date portion,
   -- and with or without the colon separators in the time portion of the string.
   function Make_ISO_8601_Date_Time (Year    : Gregorian_Year_Number;
                                     Month   : Gregorian_Month_Number;
                                     Day     : Gregorian_Day_Number;
                                     Hour    : Hour_Number;
                                     Minute  : Minute_Number;
                                     Second  : Second_Number;
                                     T_Sep   : Boolean := False;
                                     Hyphen  : Boolean := True;
                                     Colon   : Boolean := True) return ISO_8601_Date_Time_String
     with Post => (Date_Time_Format_Okay (Make_ISO_8601_Date_Time'Result, T_Sep, Hyphen, Colon) and
                       Valid_ISO_8601_Date_Time_String(Make_ISO_8601_Date_Time'Result));


   -- Extract and return the year number from an ISO 8601 date string or date/time string.
   function Extract_Year (Date_Time_String : ISO_8601_Date_Time_String) return Gregorian_Year_Number
     with
       Pre =>
         Length (Date_Time_String) >= 4 and then Contains_Digits_Only (DTB.Slice (Date_Time_String, 1, 4));


   -- Extract and return the month number from an ISO 8601 date string or date/time string.
   function Extract_Month (Date_Time_String : ISO_8601_Date_Time_String) return Gregorian_Month_Number
     with
       Pre =>
         Length (Date_Time_String) >= (if DTB.Index (Date_Time_String, "-") > 0 then 7 else 6) and then
     (if DTB.Index (Source => +(DTB.Slice (Date_Time_String, Low => 1, High => 6)), Pattern => "-") > 0 then
          Contains_Digits_Only (DTB.Slice (Date_Time_String, 6, 7))
        else
          Contains_Digits_Only (DTB.Slice (Date_Time_String, 5, 6)));


   -- Extract and return the day number from an ISO 8601 date string or date/time string.
   function Extract_Day (Date_Time_String : ISO_8601_Date_Time_String) return Gregorian_Day_Number
     with
       Pre =>
         Length (Date_Time_String) >= (if DTB.Index (Date_Time_String, "-") > 0 then 10 else 8) and then
     (if DTB.Index (Source => +(DTB.Slice (Date_Time_String, Low => 1, High => 6)), Pattern => "-") > 0 then
          Contains_Digits_Only (DTB.Slice (Date_Time_String, 9, 10))
        else
          Contains_Digits_Only (DTB.Slice (Date_Time_String, 7, 8)));


   -- Return the difference in days between Date1 and Date2,
   -- where Date1 is the earlier date and Date2 is the later date. Switching the order of the dates will yield a
   -- negative difference.
   function Date_Diff (Date1_String, Date2_String : ISO_8601_Date_String) return Date_Diff_Days;


   -- Return as a string the day of the week of the given date.
   function Day_Of_Week_String (Date_String : ISO_8601_Date_String) return String;


   -- Return as an enumeration constant the day of the week of the given date.
   function Day_Of_Week (Date_String : ISO_8601_Date_String) return DOW;


   -- Return the number of days in the given month, or 29 in the case of February.
   function Days_In_Month (Month_Num : Gregorian_Month_Number) return Gregorian_Day_Number;

end Eclecticse.Date;
