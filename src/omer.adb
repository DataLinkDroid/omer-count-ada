--pragma Elaboration_Checks (Static);

with Ada.Calendar;            use Ada.Calendar;
with Ada.Calendar.Arithmetic; use Ada.Calendar.Arithmetic;

with Ada.Text_IO;             use Ada.Text_IO;
with Ada.Characters.Handling; use Ada.Characters.Handling;

with Ada.Strings.Fixed; use Ada.Strings.Fixed;
use Ada.Strings;

with Shavuot_Banner; use Shavuot_Banner;
with English_Cardinals_Ordinals; use English_Cardinals_Ordinals;

package body Omer is


   function Today return ISO_8601_Date_String is
      (ISO_8601_Date_Now);


   function Day_Count_To (Date : ISO_8601_Date_String) return Date_Diff_Days is
      (Date_Diff(Pesach, Date));


   function Increment_Date (Date : ISO_8601_Date_String) return ISO_8601_Date_String
   is
      use DTB;
      Hyphen    : Boolean := Index (+Slice (Date, 1, 5), "-") > 0;
      Year      : Gregorian_Year_Number := Extract_Year (Date);
      Month     : Gregorian_Month_Number := Extract_Month (Date);
      Day       : Gregorian_Day_Number := Extract_Day (Date);
      Date1     : Time := Time_Of (Year, Month, Day);
      Date2     : Time := Date1 + 1;
      New_Year  : Year_Number;
      New_Month : Month_Number;
      New_Day   : Day_Number;
      Seconds   : Day_Duration;
      Morrow    : ISO_8601_Date_String;
   begin
      Split (Date2, New_Year, New_Month, New_Day, Seconds);
      if Seconds /= Duration(0) then
         -- Incredibly, Ada cannot correctly add a day!
         -- It instead adds 24 hours, which is NOT one day
         -- in the presence of daylight savings changes.
         if Seconds > Duration(43_200) then
            Date2 := Date2 + 1;
            Split (Date2, New_Year, New_Month, New_Day, Seconds);
         end if;
      end if;
      return Make_ISO_8601_Date (Gregorian_Year_Number (New_Year),
                                 Gregorian_Month_Number (New_Month),
                                 Gregorian_Day_Number (New_Day),
                                 Hyphen => Hyphen);
   end Increment_Date;


   function Is_Sabbath (Date : ISO_8601_Date_String) return Boolean is
     (Day_Of_Week (Date) = Saturday);


   function Sabbath_Count (Date : ISO_8601_Date_String) return Natural
   is
      Start_Date : ISO_8601_Date_String := Increment_Date (Pesach);
      End_Date   : ISO_8601_Date_String := Date;
      This_Date  : ISO_8601_Date_String := Start_Date;
      Count      : Natural := 0;
   begin
      while Date_Diff (This_Date, End_Date) >= 0 loop
         if Is_Sabbath (This_Date) then
            Count := Count + 1;
         end if;
         This_Date := Increment_Date (This_Date);
      end loop;
      return Count;
   end Sabbath_Count;


   ----------------
   -- Set_Pesach --
   ----------------

   procedure Set_Pesach (Date : ISO_8601_Date_String) is
   begin
      Pesach := Date;
   end Set_Pesach;

   ----------
   -- Omer --
   ----------

   procedure Omer is
   begin
      Omer_For(Today);
   end Omer;

   --------------
   -- Omer_For --
   --------------

   procedure Omer_For (Date : ISO_8601_Date_String)
   is
      Day_Count : Date_Diff_Days := Day_Count_To (Date);
      Remaining_Days : Date_Diff_Days := 50 - Day_Count;

      procedure Normal_Count
      is
         use DTB;
         Day_OW : String := Day_Of_Week_String (Date);
      begin
         Put_Line (To_Upper ("Omer count for " & (if Day_OW = "Saturday" then "Shabbat" else Day_OW) & " " &
                     To_String (Date)));
         New_Line;
         if Day_Count = 25 then
            Put_Line ("We are half-way to Shavuot!");
         end if;
         Put_Line ("It is the " & Ordinal (English_Number(Day_Count)) & " day of fifty to Shavuot.");
         Put_Line ("There " & (if Remaining_Days > 1 then "are " else "is ") &
                     Cardinal (English_Number (Remaining_Days)) & " more " &
                   (if Remaining_Days > 1 then "days" else "day") & " to go!");
         Put_Line ("It is the " & Ordinal (English_Number(((Day_Count - 1) mod 7) + 1)) & " day of the " &
                     Ordinal (English_Number(((Day_Count - 1) / 7) + 1)) & " week since Pesach.");
         declare
            Num_Sabbaths : Natural := Sabbath_Count (Date);
         begin
            if Is_Sabbath (Date) then
               Put_Line ("It is the " & Ordinal (English_Number(Num_Sabbaths)) & " of the Shabbats!");
            else
               Put_Line ("We have counted " & Cardinal (English_Number(Num_Sabbaths)) & " of the seven Shabbats!");
            end if;
         end;
      end Normal_Count;

   begin
      if Day_Count > 0 and Day_Count < 50 then
         Normal_Count;
      elsif Day_Count = 0 then
         Put_Line (DTB.To_String (Date) & " is Pesach. Fifty more days to Shavuot.");
      elsif Day_Count = 50 then
         Put_Line ("Today (" & DTB.To_String (Date) & ") is ...");
         New_Line;
         Shavuot;
      elsif Day_Count > 50 then
         Put_Line ("Sorry, but you have missed Shavuot!");
      else
         Put_Line ("It is not yet Pesach. The omer count has not yet begun.");
      end if;
   end Omer_For;

end Omer;
