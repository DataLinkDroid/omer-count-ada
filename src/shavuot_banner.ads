
-- @summary
-- Print a textual banner which reads, "SHAVUOT".
--
package Shavuot_Banner is

   -- Print Shavuot banner out to standard output.
   procedure Shavuot;

end Shavuot_Banner;
