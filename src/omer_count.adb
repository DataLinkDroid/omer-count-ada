pragma Elaboration_Checks (Static);

with Ada.Text_IO;                use Ada.Text_IO;
with Ada.Integer_Text_IO;        use Ada.Integer_Text_IO;
with Eclecticse.Date;            use Eclecticse.Date;
with Shavuot_Banner;             use Shavuot_Banner;
with Omer;                       use Omer;
with English_Cardinals_Ordinals; use English_Cardinals_Ordinals;
with GNAT.Command_Line;          use GNAT.Command_Line;


procedure Omer_Count
is
   package DTB renames Eclecticse.Date.DTB;
   use all type DTB.Bounded_String;

   For_Date      : ISO_8601_Date_String;
   Pesach_Date   : ISO_8601_Date_String;
   Do_Exit       : Boolean := False;
   Display_Usage : Boolean := False;

   procedure Usage
   is
   begin
      New_Line;
      Put_Line ("Usage:");
      Put_Line ("   omer_count [--for <ISO 8601 date>] [--pesach <ISO 8601 date>]");
      New_Line;
      Put_Line ("Examples:");
      Put_Line ("    omer_count --pesach 2019-03-23");
      Put_Line ("    omer_count --for 2019-05-14 --pesach 2019-03-23");
      New_Line;
   end Usage;


   procedure Process_Params
   is
   begin
      loop
         case Getopt ("h -for= -pesach= -help") is
            when 'h' =>
               Display_Usage := True;
               Do_Exit := True;
            when '-' =>
               if Full_Switch = "-help" then
                  Display_Usage := True;
                  Do_Exit := True;
               elsif Full_Switch = "-for" then
                  For_Date := +Parameter;
               elsif Full_Switch = "-pesach" then
                  Pesach_Date := +Parameter;
               end if;
            when others =>
               exit;
         end case;
      end loop;
   end Process_Params;

begin

   Process_Params;

   if Display_Usage then
      Usage;
   end if;

   if not Do_Exit then
      if Pesach_Date /= DTB.Null_Bounded_String then
         Set_Pesach (Pesach_Date);
      end if;
      if For_Date /= DTB.Null_Bounded_String then
         Omer_For (For_Date);
      else
         Omer.Omer;
      end if;
   end if;

end Omer_Count;
