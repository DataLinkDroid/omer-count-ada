with Eclecticse.Date; use Eclecticse.Date;

-- @summary
-- Print out an omer count for today or another given day.
--
-- @description
-- This package provides two routines for printing out an omer count
-- for some day between Pesach and Shavuot. The date for Pesach (15
-- Aviv) first needs to be set, and then Omer procedure can be called
-- to print out the count for today (the current day), or the Omer_For
-- procedure can be called to print the count for any other day
-- between Pesach and Shavuot.
package Omer is

   -- Set the Gregorian date for Pesach (15 Aviv) which we want to use.
   -- To minimise confusion, always use the day portion of 15 Aviv in
   -- determining the corresponding Gregorian date.
   -- @param Date The date provided is expected to be in YYYY-MM-DD
   -- ISO 8601 date format.
   procedure Set_Pesach (Date : ISO_8601_Date_String);


   -- Print the omer count for the current day ("today").
   procedure Omer;


   -- Print the omer count for the given date.
   -- @param Date The date provided is expected to be in YYYY-MM-DD
   -- ISO 8601 date format.
   procedure Omer_For (Date : ISO_8601_Date_String);

private

   -- The date of Pesach used internally as the starting base for
   -- counting.
   Pesach : ISO_8601_Date_String := +"2023-04-06";

end Omer;
