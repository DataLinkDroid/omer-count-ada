with Ada.Calendar.Arithmetic;
with Ada.Calendar.Formatting;
with Ada.Integer_Text_IO;


package body Eclecticse.Date is

   package ACF renames Ada.Calendar.Formatting;

   function Padded_Value (The_String : String; Length : Positive) return String is
   begin
      return SF.Tail (SF.Trim (Source => The_String,
                               Side   => AS.Both),
                      Count => Length,
                      Pad   => '0');
   end Padded_Value;


   function "+" (S : String) return DTB.Bounded_String is
     (DTB.To_Bounded_String(S));


   function "-" (S : DTB.Bounded_String) return String is
     (DTB.To_String(S));


   function Ada_Day_To_DOW (Ada_Day : ACF.Day_Name) return DOW
   is
      use ACF;
   begin
      return (case Ada_Day is
                 when Sunday => Sunday,
                 when Monday => Monday,
                 when Tuesday => Tuesday,
                 when Wednesday => Wednesday,
                 when Thursday => Thursday,
                 when Friday => Friday,
                 when Saturday => Saturday);
   end Ada_Day_To_DOW;


   function DOW_To_DOW_String (Day : DOW) return String is
     (case Day is
         when Sunday => "Sunday",
         when Monday => "Monday",
         when Tuesday => "Tuesday",
         when Wednesday => "Wednesday",
         when Thursday => "Thursday",
         when Friday => "Friday",
         when Saturday => "Saturday");


   function Days_In_Month (Month_Num : Gregorian_Month_Number) return Gregorian_Day_Number
     is (case Month_Num is
            when 9 | 4 | 6 | 11 => 30,
            when 1 | 3 | 5 | 7 | 8 | 10 | 12 => 31,
            when 2 => 29);


   function Date_Format_Okay
     (The_Date_String : ISO_8601_Date_String;
      With_Hyphens    : Boolean := True)
     return Boolean
   is
   begin
      return Good_Format : Boolean do
         if With_Hyphens then
            Good_Format := Length (The_Date_String) = 10 and
              (Index (The_Date_String, ASM.To_Set ("0123456789-"), AS.Outside) = 0) and
              Count (The_Date_String, ASM.To_Set ('-')) = 2;
         else
            Good_Format := Length (The_Date_String) = 8 and
              (Index (The_Date_String, ASM.To_Set (Span => ('0', '9')), AS.Outside) = 0);
         end if;
      end return;
   end Date_Format_Okay;


   function Time_Format_Okay
     (The_Time_String : ISO_8601_Time_String;
      With_Colons     : Boolean := True)
     return Boolean
   is
   begin
      return Good_Format : Boolean do
         if With_Colons then
            Good_Format :=
              Length (The_Time_String) = 8 and
                (Index (The_Time_String, ASM.To_Set ("0123456789:"), AS.Outside) = 0) and
                Count (The_Time_String, ASM.To_Set (':')) = 2;
         else
            Good_Format :=
              (Length (The_Time_String) = 6) and
                 (Index (The_Time_String, ASM.To_Set (Span => ('0', '9')), AS.Outside) = 0);
         end if;
      end return;
   end Time_Format_Okay;


   function Date_Time_Format_Okay
     (The_Date_Time_String : ISO_8601_Date_Time_String;
      With_T_Sep           : Boolean := False;
      With_Hyphens         : Boolean := True;
      With_Colons          : Boolean := True)
     return Boolean
   is
      Sep_Pos : Natural :=
        Index (The_Date_Time_String, ASM.To_Set (if With_T_Sep then "T" else " "), AS.Inside);
      My_Date_String : ISO_8601_Date_String :=
        +DTB.Slice (The_Date_Time_String, 1, (if Sep_Pos = 0 then Sep_Pos else Sep_Pos - 1));
      My_Time_String : ISO_8601_Time_String :=
        (if Sep_Pos = 0 then
            +""
         else
            +DTB.Slice (The_Date_Time_String, Sep_Pos + 1, Length (The_Date_Time_String)));
   begin
      return Good_Format : Boolean do
         Good_Format := Date_Format_Okay (My_Date_String, With_Hyphens) and
           Time_Format_Okay (My_Time_String, With_Colons);
      end return;
   end Date_Time_Format_Okay;


   function Contains_Digits_Only (The_String : String) return Boolean is
     (for all C in The_String'Range => The_String (C) in '0' .. '9');


   -- Return True if the given string represents a valid ISO 8601 date
   -- or date/time string. This function does not check that the date
   -- or date/time actually exists (for example, it will not check
   -- that a date of 29 February is in a leap year. However, component
   -- values are range checked so that only allowed values for year,
   -- month, day, hour, minute, second and time zone offset are
   -- present. The date part must represent a full year/month/day: a
   -- year only, or a year and month only, is not permitted. The time
   -- part, if present, must also have all of the hour, minute and
   -- second components. The time zone offset component, if present,
   -- must have both hour and minute components.
   function Valid_ISO_8601_Date_Time_String
     (The_Date_Time_String : ISO_8601_Date_Time_String)
     return Boolean
   is
      use Ada.Strings;

      -- Utilities

      function Valid_TZ (TZ_Hour_String, TZ_Min_String : String) return Boolean
      is
         pragma Unsuppress (Range_Check);
         TZ_Hour_Num : Natural range 0 .. 28 := 0;
         TZ_Min_Num  : Natural range 0 .. 59 := 0;
      begin
         TZ_Hour_Num := Natural'Value (TZ_Hour_String);
         TZ_Min_Num := Natural'Value (TZ_Min_String);
         return True;
      exception
         when Constraint_Error =>
            return False;
      end Valid_TZ;

      function Contains (Str : String; Char : Character) return Boolean is
        (for some C of Str => C = Char) with Inline;

      function Contains_Hyphen (Str : String) return Boolean is
        (Contains (Str, '-')) with Inline;

      function Contains_T (Str : String) return Boolean is
        (Contains (Str, 'T')) with Inline;

      function Contains_Colon (Str : String) return Boolean is
        (Contains (Str, ':')) with Inline;

      function Clamp_Index
        (The_Index : Natural;
         The_String : ISO_8601_Date_Time_String) return Natural
        is (Natural'Min (The_Index, Length (The_String))) with Inline;

      function Good_Frac_Length (The_Length : Natural) return Boolean is
        (The_Length = 4) with Inline;

      function Is_Decimal_Mark (The_Char : Character) return Boolean is
        (The_Char = '.') with Inline;

      function Good_TZ_Component_Length (The_Length : Natural) return Boolean is
        (The_Length = 2) with Inline;

      function TZ_Position
        (The_Date_Time : DTB.Bounded_String;
         Start_From : Positive) return Natural
        is (Index (Source => The_Date_Time,
                   From => Start_From,
                   Set => ASM.To_Set ("+-")))
      with Inline;

      -- End utilities


      -- Date part contains hyphens.
      Hyphen : Boolean :=
        Contains_Hyphen (Slice (The_Date_Time_String, 1, Clamp_Index (10, The_Date_Time_String)));

      -- A T separator exists between the date part and the time part.
      T_Sep : Boolean :=
        Contains_T(Slice (The_Date_Time_String, 1, Clamp_Index (11, The_Date_Time_String)));

      -- The time part contains colons.
      Colon : Boolean :=
        (if Length (The_Date_Time_String) < 14 then
            False
         else
            Contains_Colon (Slice (The_Date_Time_String, 12, 14)));

      -- The seconds part includes a factional value.
      Sec_Frac : Boolean :=
        (DTB.Index (Source => The_Date_Time_String, From => 1, Pattern => ".") /= 0);

      -- The index position of the time zone part.
      TZ_Pos : Natural :=
        TZ_Position (The_Date_Time_String,
                     Start_From => (if Length (The_Date_Time_String) > 8
                                    then 9
                                    else 1));

      -- The index position of the T or space separator between the date part and the time part.
      Sep_Pos : Natural :=
        Index (The_Date_Time_String, ASM.To_Set (if T_Sep then "T" else " "), AS.Inside);

      -- There is a time part.
      Has_Time : Boolean := Sep_Pos /= 0;

      -- There is a time zone part.
      Has_TZ : Boolean := TZ_Pos /= 0;

      -- The date part is good.
      Good_Date : Boolean := False;

      -- The time part is good.
      Good_Time : Boolean := False;

      -- The time zone offset part is good.
      Good_TZ_Off : Boolean := False;

      -- The date part of the date/time string.
      Date_Part : ISO_8601_Date_String;

      -- The time part of the date/time string.
      Time_Part : DTB.Bounded_String;

      -- The overall format is good, without regard to value ranges.
      Good_Format : Boolean := False;

   begin

      -- Separate into the date part and the time part.
      if Has_Time then
         Date_Part := +DTB.Slice (The_Date_Time_String,
                                  1,
                                  (if Sep_Pos = 0 then Sep_Pos else Sep_Pos - 1));
         Time_Part := +DTB.Slice (The_Date_Time_String,
                                  Sep_Pos + 1,
                                  Length (The_Date_Time_String));
      else
         Date_Part := The_Date_Time_String;
         Time_Part := +"";
      end if;

      -- Determine if the overall formatting of the date/time is
      -- correct, without regard to range checks.
      if not Has_Time then
         Good_Format := Date_Format_Okay (Date_Part, Hyphen);

      elsif not Sec_Frac and not Has_TZ then
         Good_Format :=
           Date_Format_Okay (Date_Part, Hyphen) and then
           Time_Format_Okay (Time_Part, Colon);

      elsif Sec_Frac and not Has_TZ then
         declare
            Frac_Part : String := Slice (Time_Part,
                                         (if Colon then 10 else 8),
                                         Length (Time_Part));
         begin
            Good_Format :=
              Date_Format_Okay (Date_Part, Hyphen) and then
              Time_Format_Okay (+Slice (Time_Part, 1, (if Colon then 8 else 6)), Colon) and then
              Is_Decimal_Mark (DTB.Element (Time_Part, (if Colon then 9 else 7))) and then
              Good_Frac_Length (Frac_Part'Length) and then
              Contains_Digits_Only(Frac_Part);
         end;

      elsif Sec_Frac and Has_TZ then
         declare
            use Ada.Strings.Fixed;
            TZ_Pos_Date    : Natural renames TZ_Pos;
            TZ_Pos         : Natural := TZ_Position (Time_Part, Start_From => 8);
            Frac_Part      : String := Slice (Time_Part, (if Colon then 10 else 8), TZ_Pos - 1);
            TZ_Part        : String := Slice (The_Date_Time_String,
                                              TZ_Pos_Date + 1,
                                              Length (The_Date_Time_String));
            TZ_Colon_Pos   : Natural := Index (Source => TZ_Part, Pattern => ":");
            Has_TZ_Colon   : Boolean := TZ_Colon_Pos > 0;
            TZ_Hour_String : String := TZ_Part (TZ_Part'First .. TZ_Part'First + 1);
            TZ_Min_String  : String := TZ_Part ((if Has_TZ_Colon then
                                                    TZ_Colon_Pos + 1
                                                 else
                                                    TZ_Part'First + 2) .. TZ_Part'Last);
         begin
            Good_Format :=
              Date_Format_Okay (Date_Part, Hyphen) and then
              Time_Format_Okay (+Slice (Time_Part, 1, (if Colon then 8 else 6)), Colon) and then
              Is_Decimal_Mark (DTB.Element (Time_Part, (if Colon then 9 else 7))) and then
              Good_Frac_Length (Frac_Part'Length) and then
              Contains_Digits_Only (Frac_Part) and then
              Good_TZ_Component_Length (TZ_Hour_String'Length) and then
              Good_TZ_Component_Length (TZ_Min_String'Length) and then
              Contains_Digits_Only (TZ_Hour_String) and then
              Contains_Digits_Only (TZ_Min_String);
            if Good_Format then
               Good_TZ_Off := Valid_TZ (TZ_Hour_String, TZ_Min_String);
            end if;
         end;

      elsif not Sec_Frac and Has_TZ then
         declare
            use Ada.Strings.Fixed;

            -- TZ_Part is that part after the '+' or '-'.
            TZ_Part        : String := Slice (The_Date_Time_String,
                                              TZ_Pos + 1,
                                              Length (The_Date_Time_String));
            TZ_Colon_Pos   : Natural := Index (Source => TZ_Part, Pattern => ":");
            Has_TZ_Colon   : Boolean := TZ_Colon_Pos > 0;
            TZ_Hour_String : String := TZ_Part (TZ_Part'First .. TZ_Part'First + 1);
            TZ_Min_String  : String := TZ_Part ((if Has_TZ_Colon then
                                                    TZ_Colon_Pos + 1
                                                 else
                                                    TZ_Part'First + 2) .. TZ_Part'Last);
         begin
            Good_Format :=
              Date_Format_Okay (Date_Part, Hyphen) and then
              Time_Format_Okay (+Slice (Time_Part, 1, (if Colon then 8 else 6)), Colon) and then
              Good_TZ_Component_Length (TZ_Hour_String'Length) and then
              Good_TZ_Component_Length (TZ_Min_String'Length) and then
              Contains_Digits_Only (TZ_Hour_String) and then
              Contains_Digits_Only (TZ_Min_String);
            if Good_Format then
               Good_TZ_Off := Valid_TZ (TZ_Hour_String, TZ_Min_String);
            end if;
         end;
      end if;

      -- Determine if the ranges of the date/time components are valid or not.
      if Good_Format then
         declare
            pragma Unsuppress (Range_Check);
            Y_Str       : String := Slice (Date_Part, 1, 4);
            M_Str       : String := Slice (Date_Part,
                                           (if Hyphen then 6 else 5),
                                           (if Hyphen then 7 else 6));
            D_Str       : String := Slice (Date_Part,
                                           (if Hyphen then 9 else 7),
                                           (if Hyphen then 10 else 8));
            Hour_Str    : String := (if Has_Time then
                                        Slice (Time_Part, 1, 2)
                                     else
                                        "");
            Min_Str     : String := (if Has_Time then
                                        Slice (Time_Part,
                                              (if Colon then 4 else 3),
                                              (if Colon then 5 else 4))
                                     else
                                        "");
            Sec_Str     : String := (if Has_Time then
                                        Slice (Time_Part,
                                              (if Colon then 7 else 5),
                                              (if Colon then 8 else 6))
                                     else
                                        "");
            Year_Num    : Gregorian_Year_Number;
            Month_Num   : Gregorian_Month_Number;
            Day_Num     : Gregorian_Day_Number;
            Hour_Num    : Hour_Number;
            Min_Num     : Minute_Number;
            Sec_Num     : Second_Number;
         begin
            Year_Num := Gregorian_Year_Number'Value (Y_Str);
            Month_Num := Gregorian_Month_Number'Value (M_Str);
            Day_Num := Gregorian_Day_Number'Value (D_Str);

            Good_Date := Days_In_Month (Month_Num) >= Day_Num;

            if Has_Time then
               Hour_Num := Hour_Number'Value (Hour_Str);
               Min_Num := Minute_Number'Value (Min_Str);
               Sec_Num := Second_Number'Value (Sec_Str);
               Good_Time := True;
            end if;

         exception
            when Constraint_Error =>
               null;
         end;
      end if;

      return
        Good_Format and then
        Good_Date and then
        (if Has_Time then Good_Time else True) and then
        (if Has_TZ then Good_TZ_Off else True);

   end Valid_ISO_8601_Date_Time_String;





   -----------------------
   -- ISO_8601_Date_Now --
   -----------------------

   function ISO_8601_Date_Now
     (Hyphen : Boolean := True)
     return ISO_8601_Date_String
   is
      use Ada.Calendar;
      Now           : Time;
      Result_String : ISO_8601_Date_String;
      Year_String   : String (1 .. Year_Digits);
      Month_String  : String (1 .. Month_Digits);
      Day_String    : String (1 .. Day_Digits);
   begin
      Now := Clock;

      Year_String := Padded_Value (Integer'Image (Year (Now)), Year_Digits);
      Month_String := Padded_Value (Integer'Image (Month (Now)), Month_Digits);
      Day_String := Padded_Value (Integer'Image (Day (Now)), Day_Digits);

      Result_String :=
        + (Year_String & (if Hyphen then "-" else "") &
             Month_String & (if Hyphen then "-" else "") &
             Day_String);

      return Result_String;
   end ISO_8601_Date_Now;

   -----------------------
   -- ISO_8601_Time_Now --
   -----------------------

   function ISO_8601_Time_Now
     (Colon : Boolean := True)
     return ISO_8601_Time_String
   is
      use Ada.Calendar;
      Now           : Time := Clock;
      Now_Seconds   : Day_Duration := Seconds (Now);
      Hour          : Natural := Integer (Now_Seconds) / Secs_In_Hour;
      Minute        : Natural := (Integer (Now_Seconds) rem Secs_In_Hour) / Secs_In_Minute;
      Second        : Natural := (Integer (Now_Seconds) rem Secs_In_Hour) rem Secs_In_Minute;
      Hour_String   : String (1 .. Hour_Digits) := Padded_Value (Integer'Image (Hour), Hour_Digits);
      Minute_String : String (1 .. Minute_Digits) := Padded_Value (Integer'Image (Minute), Minute_Digits);
      Second_String : String (1 .. Second_Digits) := Padded_Value (Integer'Image (Second), Second_Digits);
      Result_String : ISO_8601_Time_String :=
        + (Hour_String & (if Colon then ":" else "") &
             Minute_String & (if Colon then ":" else "") &
             Second_String);
   begin
      return Result_String;
   end ISO_8601_Time_Now;

   ----------------------------
   -- ISO_8601_Date_Time_Now --
   ----------------------------

   function ISO_8601_Date_Time_Now
     (T_Sep  : Boolean := False;
      Hyphen : Boolean := True;
      Colon  : Boolean := True)
     return ISO_8601_Date_Time_String
   is
   begin
      return ISO_8601_Date_Now(Hyphen => Hyphen) & (if T_Sep then "T" else " ") & ISO_8601_Time_Now(Colon => Colon);
   end ISO_8601_Date_Time_Now;

   ------------------------
   -- Make_ISO_8601_Date --
   ------------------------

   function Make_ISO_8601_Date
     (Year   : Gregorian_Year_Number;
      Month  : Gregorian_Month_Number;
      Day    : Gregorian_Day_Number;
      Hyphen : Boolean := True)
     return ISO_8601_Date_String
     is
     (+(Padded_Value (Integer'Image (Year), Year_Digits) & (if Hyphen then "-" else "") &
          Padded_Value (Integer'Image (Month), Month_Digits) & (if Hyphen then "-" else "") &
          Padded_Value (Integer'Image (Day), Day_Digits)));

   ------------------------
   -- Make_ISO_8601_Time --
   ------------------------

   function Make_ISO_8601_Time
     (Hour    : Hour_Number;
      Minute  : Minute_Number;
      Second  : Second_Number;
      Colon   : Boolean := True)
     return ISO_8601_Time_String
     is
     (+(Padded_Value (Integer'Image (Hour), Hour_Digits) & (if Colon then ":" else "") &
          Padded_Value (Integer'Image (Minute), Minute_Digits) & (if Colon then ":" else "") &
          Padded_Value (Integer'Image (Second), Second_Digits)));

   -----------------------------
   -- Make_ISO_8601_Date_Time --
   -----------------------------

   function Make_ISO_8601_Date_Time
     (Year    : Gregorian_Year_Number;
      Month   : Gregorian_Month_Number;
      Day     : Gregorian_Day_Number;
      Hour    : Hour_Number;
      Minute  : Minute_Number;
      Second  : Second_Number;
      T_Sep   : Boolean := False;
      Hyphen  : Boolean := True;
      Colon   : Boolean := True)
     return ISO_8601_Date_Time_String
     is
     (Make_ISO_8601_Date (Year, Month, Day, Hyphen) & (if T_Sep then "T" else " ") &
        Make_ISO_8601_Time (Hour, Minute, Second, Colon));

   ------------------
   -- Extract_Year --
   ------------------

   function Extract_Year
     (Date_Time_String : ISO_8601_Date_Time_String)
     return Gregorian_Year_Number
   is
      Year_String  : String (1 .. Year_Digits) := Slice (Date_Time_String, 1, 4);
      Parse_Result : Integer;
      Year_Number  : Gregorian_Year_Number;
      Last_At      : Positive;
   begin
      Ada.Integer_Text_IO.Get (From => Year_String, Item => Parse_Result, Last => Last_At);
      if Parse_Result not in Gregorian_Year_Number'Range then
         raise Bad_Year with
           "Year " & Year_String & " is not in Gregorian year range " &
           SF.Trim (Integer'Image (Gregorian_Year_Number'First), Side => AS.Both) &
           " .. " &
           SF.Trim (Integer'Image (Gregorian_Year_Number'Last), Side => AS.Both);
      end if;
      Year_Number := Parse_Result;
      return Year_Number;
   end Extract_Year;

   -------------------
   -- Extract_Month --
   -------------------

   function Extract_Month
     (Date_Time_String : ISO_8601_Date_Time_String)
     return Gregorian_Month_Number
   is
      First_Hyphen_Pos : Integer := Index (Date_Time_String, "-");
      Month_Start_Pos  : Integer := (if First_Hyphen_Pos = 0 then 5 else
                                       (if First_Hyphen_Pos < 6 then 6 else 5));
      Month_End_Pos    : Integer := Month_Start_Pos + 1;
      Month_String     : String (1 .. Month_Digits) := Slice (Date_Time_String, Month_Start_Pos, Month_End_Pos);
      Parse_Result     : Integer;
      Month_Number     : Gregorian_Month_Number;
      Last_At          : Positive;
   begin
      Ada.Integer_Text_IO.Get (From => Month_String, Item => Parse_Result, Last => Last_At);
      if Parse_Result not in Gregorian_Month_Number'Range then
         raise Bad_Month with
           "Month " & Month_String & " is not in Gregorian month range " &
           SF.Trim (Integer'Image (Gregorian_Month_Number'First), Side => AS.Both) &
           " .. " &
           SF.Trim (Integer'Image (Gregorian_Month_Number'Last), Side => AS.Both);
      end if;
      Month_Number := Parse_Result;
      return Month_Number;
   end Extract_Month;

   -----------------
   -- Extract_Day --
   -----------------

   function Extract_Day
     (Date_Time_String : ISO_8601_Date_Time_String)
     return Gregorian_Day_Number
   is
      Second_Hyphen_Pos : Integer := Index (Date_Time_String, "-", From => 6);
      Day_Start_Pos     : Integer := (if Second_Hyphen_Pos = 0 then 7 else
                                        (if Second_Hyphen_Pos < 9 then 9 else 7));
      Day_End_Pos       : Integer := Day_Start_Pos + 1;
      Day_String        : String (1 .. Day_Digits) := Slice (Date_Time_String, Day_Start_Pos, Day_End_Pos);
      Parse_Result      : Integer;
      Day_Number        : Gregorian_Day_Number;
      Last_At           : Positive;
   begin
      Ada.Integer_Text_IO.Get (From => Day_String, Item => Parse_Result, Last => Last_At);
      if Parse_Result not in Gregorian_Day_Number'Range then
         raise Bad_Day with
           "Day " & Day_String & " is not in Gregorian day range " &
           SF.Trim (Integer'Image (Gregorian_Day_Number'First), Side => AS.Both) &
           " .. " &
           SF.Trim (Integer'Image (Gregorian_Day_Number'Last), Side => AS.Both);
      end if;
      Day_Number := Parse_Result;
      return Day_Number;
   end Extract_Day;

   ---------------
   -- Date_Diff --
   ---------------

   function Date_Diff
     (Date1_String, Date2_String : ISO_8601_Date_String)
     return Date_Diff_Days
   is
      use Ada.Calendar.Arithmetic;
      use Ada.Calendar;
      Year1        : Gregorian_Year_Number := Extract_Year (Date1_String);
      Month1       : Gregorian_Month_Number := Extract_Month (Date1_String);
      Day1         : Gregorian_Day_Number := Extract_Day (Date1_String);
      Year2        : Gregorian_Year_Number := Extract_Year (Date2_String);
      Month2       : Gregorian_Month_Number := Extract_Month (Date2_String);
      Day2         : Gregorian_Day_Number := Extract_Day (Date2_String);
      Date1        : Time := Time_Of (Year1, Month1, Day1);
      Date2        : Time := Time_Of (Year2, Month2, Day2);
      Days         : Day_Count;
      Seconds      : Duration;
      Leap_Seconds : Leap_Seconds_Count;
   begin
      Difference (Date2, Date1, Days, Seconds, Leap_Seconds);
      if Integer (Days) not in Date_Diff_Days'Range then
         raise Bad_Date_Difference with "Difference between the two dates is too great!";
      end if;
      return Date_Diff_Days(Days);
   end Date_Diff;

   -----------------
   -- Day_Of_Week --
   -----------------

   function Day_Of_Week
     (Date_String : ISO_8601_Date_String)
     return DOW
   is
      use Ada.Calendar;
      use ACF;
      Year         : Gregorian_Year_Number := Extract_Year (Date_String);
      Month        : Gregorian_Month_Number := Extract_Month (Date_String);
      Day          : Gregorian_Day_Number := Extract_Day (Date_String);
      Ada_Day_Name : Day_Name;
   begin
      Ada_Day_Name := Day_Of_Week (Ada.Calendar.Time_Of (Year, Month, Day));
      return Ada_Day_To_DOW(Ada_Day_Name);
   end Day_Of_Week;

   ------------------------
   -- Day_Of_Week_String --
   ------------------------

   function Day_Of_Week_String
     (Date_String : ISO_8601_Date_String)
     return String
     is
     (DOW_To_DOW_String(Day_Of_Week(Date_String)));

end Eclecticse.Date;
