with Ada.Text_IO;

package body Shavuot_Banner is

   type Letter_Line is new Integer range 1 .. 6;
   type Letter_Line_String is new String(1 .. 9);
   type Letter_Definition is array (Letter_Line) of Letter_Line_String;

   package TIO renames Ada.Text_IO;
   procedure New_Line (Spacing : TIO.Positive_Count := 1) renames TIO.New_Line;
   procedure Put(Item : String) renames TIO.Put;


   procedure Es (Line : Letter_Line);

   procedure H (Line : Letter_Line);

   procedure A (Line : Letter_Line);

   procedure Vee (Line : Letter_Line);

   procedure U (Line : Letter_Line);

   procedure O (Line : Letter_Line);

   procedure Tee (Line : Letter_Line);

   Letter_S : constant Letter_Definition :=
                ("SSSSSSS  ",
                 "S        ",
                 "SSSSSSS  ",
                 "SSSSSSS  ",
                 "      S  ",
                 "SSSSSSS  ");

   Letter_H : constant Letter_Definition :=
                ("H    H   ",
		 "H    H   ",
		 "HHHHHH   ",
		 "HHHHHH   ",
		 "H    H   ",
		 "H    H   ");

   Letter_A : constant Letter_Definition :=
                ("   A     ",
		 "  A A    ",
		 " A   A   ",
		 "AAAAAAA  ",
		 "A     A  ",
		 "A     A  ");

   Letter_V : constant Letter_Definition :=
                ("V     V  ",
		 " V   V   ",
		 " V   V   ",
		 " V   V   ",
		 "  V V    ",
		 "   V     ");

   Letter_U : constant Letter_Definition :=
                ("U     U  ",
		 "U     U  ",
		 "U     U  ",
		 "U     U  ",
		 "U     U  ",
		 " UUUUU   ");

   Letter_O : constant Letter_Definition :=
                (" OOOOO   ",
		 "O     O  ",
		 "O     O  ",
		 "O     O  ",
		 "O     O  ",
		 " OOOOO   ");

   Letter_T : constant Letter_Definition :=
                ("TTTTTTT  ",
                 "   T     ",
                 "   T     ",
                 "   T     ",
                 "   T     ",
                 "   T     ");



   -------------
   -- Shavuot --
   -------------

   procedure Shavuot is
   begin
      for Line in Letter_Line'Range loop
         Es(Line); H(Line); A(Line); Vee(Line); U(Line); O(Line); Tee(Line); New_Line;
      end loop;
      New_Line;
   end Shavuot;

   --------
   -- Es --
   --------

   procedure Es (Line : Letter_Line) is
   begin
      Put(Item => String(Letter_S(Line)));
   end Es;

   -------
   -- H --
   -------

   procedure H (Line : Letter_Line) is
   begin
      Put(Item => String(Letter_H(Line)));
   end H;

   -------
   -- A --
   -------

   procedure A (Line : Letter_Line) is
   begin
      Put(Item => String(Letter_A(Line)));
   end A;

   ---------
   -- Vee --
   ---------

   procedure Vee (Line : Letter_Line) is
   begin
      Put(Item => String(Letter_V(Line)));
   end Vee;

   -------
   -- U --
   -------

   procedure U (Line : Letter_Line) is
   begin
      Put(Item => String(Letter_U(Line)));
   end U;

   -------
   -- O --
   -------

   procedure O (Line : Letter_Line) is
   begin
      Put(Item => String(Letter_O(Line)));
   end O;

   ---------
   -- Tee --
   ---------

   procedure Tee (Line : Letter_Line) is
   begin
      Put(Item => String(Letter_T(Line)));
   end Tee;

end Shavuot_Banner;
